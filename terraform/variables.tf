variable "resource_prefix" {
  type    = "string"
  default = "hic"
}

variable "location" {
  type    = "string"
  default = "west europe"
}

variable "tags" {
  type = "map"

  default = {
    terrafrommanaged = "true"
  }
}

variable "k8s_service_principal_object_id" {
  type = "string"

  default = "00000000-0000-0000-0000-000000000000"
}
