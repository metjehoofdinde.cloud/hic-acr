resource "azurerm_resource_group" "acr" {
  name     = "${var.resource_prefix}-acr"
  location = "${var.location}"
}

resource "azurerm_container_registry" "k8s" {
  name                = "${var.resource_prefix}acrk8s"
  resource_group_name = "${azurerm_resource_group.acr.name}"
  location            = "${azurerm_resource_group.acr.location}"
  sku                 = "Basic"
}

resource "azurerm_role_assignment" "k8s-sp" {
  scope                = "${azurerm_container_registry.k8s.id}"
  role_definition_name = "Reader"
  principal_id         = "${var.k8s_service_principal_object_id}"
}
